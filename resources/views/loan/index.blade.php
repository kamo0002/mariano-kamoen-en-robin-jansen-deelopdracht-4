@extends('layouts.app')

@section('title')
	Uitleenadministratie
	<div style="float:right">
		<a class="btn btn-primary" href="{!! url('loan/create') !!}">
			<i class="fa fa-plus"></i> Lening Toevoegen
		</a>
	</div>
@endsection

@section('content')
		<?php
 			use Carbon\Carbon;
		?>
	@if (count($loans) > 0)
		<table class="table table-striped table-hover">
			<thead>
				<th class="col-sm-2">Id</th>
				<th class="col-sm-2">Uitleendatum</th>
				<th class="col-sm-2">Uitgeleend tot</th>
				<th class="col-sm-3">Gebruiker</th>
				<th class="col-sm-3">Titel</th>

			</thead>
			<tbody>
				@foreach ($loans as $loan)
				<tr class="row-link" style="cursor: pointer;"
					data-href="{{action('LoanController@show', ['id' => $loan->id]) }}">
					<td class="table-text">{{ $loan->id }}</td>
					<td class="table-text">{{ $loan->startdate }}</td>
					<td class="table-text">{{ $loan->expirydate }}</td>
					<td class="table-text">
						@if (isset($loan->user))
							{{ $loan->user->name }}
						@endif
					</td>
					<td class="table-text">
						@if (isset($loan->copy))
							{{ $loan->copy->book->title }}
						@endif
					</td>
					<td class="table-text">{!! Form::open(['route' => ['loan.handin', $loan->id], 'method'=>'put']) !!}
					                            {!! Form::button('<i class="fa fa-bars"></i>&nbspBoek inleveren', array('type' => 'submit', 'class'=> 'btn btn-success', 'onclick'=>'return confirm("Weet je zeker dat je het boek wilt inleveren?")')); !!}
					                            {!! Form::close() !!}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@endif
@endsection
@section('scripts')
<script>
	jQuery(document).ready(function($) {
	    $(".row-link").click(function() {
	        window.document.location = $(this).data("href");
	    });
	    $('#cohort-tabs a:first').tab('show') // Select first tab
	});
</script>
@endsection
