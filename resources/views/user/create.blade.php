@extends('layouts.app')

@section('title')
	Nieuwe gebruiker toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
{!! Form::open(['route' => ['user.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-12">
		{!! Form::label('name', 'Naam*', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'John Doe']) !!}
	</div>
	<div>
		<ul>
			<strong><li id="name_error" style="display:none" pattern="^[a-zA-Z]$"></li></strong>
		</ul>
	</div>
	<div class="col-sm-12">
		{!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
		{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'gebruiker@domein.nl']) !!}
	</div>
	<div>
		<ul>
			<strong><li id="email_error" style="display:none"></li></strong>
		</ul>
	</div>
	<div class="col-sm-12">
		{!! Form::label('role_id', 'Rol*', ['class' => 'control-label']) !!}
		{!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
	</div>
	<div class="col-sm-12">
		{!! Form::label('location_id', 'Locatie', ['class' => 'control-label']) !!}
		{!! Form::select('location_id', $locations, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}

@endsection
@section('scripts')
<script type="text/javascript">
document.getElementById("name").onchange = function() {
	nameValidate()
};
function nameValidate() {
	var name = document.getElementById("name").value;
	if(name.length <= 40 && name.length >= 7){
		NameCheck= "Naam in orde"
		document.getElementById("name_error").style.display= "block";
		document.getElementById("name_error").style.color= "green";
		document.getElementById("name_error").innerHTML = NameCheck;
		}
		else {
		NameCheck2= "Voer een geldige naam in."
		document.getElementById("name_error").style.display= "block";
		document.getElementById("name_error").style.color= "red";
		document.getElementById("name_error").innerHTML = NameCheck2
		}
	{
	if (name.indexOf(' ') == -1) {
			NameError= "De naam moet minimaal uit een voor en achternaam bestaan."
			document.getElementById("name_error").style.display = "block";
			document.getElementById("name_error").style.color= "red";
			document.getElementById("name_error").innerHTML = NameError;
		}
	}
}
document.getElementById("email").onchange = function() {
	emailValidate()
};
function emailValidate() {
	var email = document.getElementById("email").value;
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf (".");
	if(email.length > 5){
	EmailCheck= "Het email adres is in orde"
	document.getElementById("email_error").style.display= "block";
	document.getElementById("email_error").style.color= "green";
	document.getElementById("email_error").innerHTML = EmailCheck;
		}
	{
	if (email.indexOf(' ') >= 0) {
			EmailError= "Het email adres mag geen spaties bevatten."
			document.getElementById("email_error").style.display = "block";
			document.getElementById("email_error").style.color= "red";
			document.getElementById("email_error").innerHTML = EmailError;
		}
	else if (atpos == -1) {
			EmailError2= "Het email adres moet minimaal een @ bevatten."
			document.getElementById("email_error").style.display = "block";
			document.getElementById("email_error").style.color= "red";
			document.getElementById("email_error").innerHTML = EmailError2;
		}
	else if (email.indexOf('.') == -1) {
			EmailError3= "Het emailadres moet minimaal een . bevatten."
			document.getElementById("email_error").style.display = "block";
			document.getElementById("email_error").style.color= "red";
			document.getElementById("email_error").innerHTML = EmailError3;
		}
	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
			EmailError4= "Voer een geldig email adres in"
			document.getElementById("email_error").style.display = "block";
			document.getElementById("email_error").style.color= "red";
			document.getElementById("email_error").innerHTML = EmailError4;
		}
	}
}
</script>
@endsection
